import React, {cloneElement, Component} from 'react';
import axios from 'axios';



class Cartes extends React.Component{
    render() {
        const {name, attack, defense} = this.props
        const link = 'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/'+ name +'_0.jpg'
        return (
            <div className="card carte">
                    <img className="card-img-top" src={link}/>
                <div className="card-body ">
                    <h5 className=" taille card-title">{name}</h5>
                    <h5 className="taille text-muted card-title">Att : {attack}</h5>
                    <h5 className="taille text-muted card-title">Def : {defense}</h5>
                </div>
            </div>
            
        )
    }
}

export default class Jeux extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            compteur: 0
    }
    
}

    componentDidMount(){
        axios.get('https://los.ling.fr/cards')
        .then(rep => {
            this.setState({champion : rep.data})
        }).catch()

    }

    card(){
        
        const final = []
        let v = 0
        const champ = this.state.champion
        for(let prop in champ){
            final.push(
            <a value="0" className="" value={champ[prop]['key']} id={champ[prop]['key']} className="col-md-3" href="" 
            onClick={
                t => {
                    t.preventDefault()
                    if(this.state.compteur > 19 && v == 0){
                        v = 1
                        let btn = document.createElement("button")
                        btn.textContent = 'Valider'
                        btn.className = "btn btn-primary"
                        document.getElementById('buton').appendChild(btn)
                        

                    }else if (this.state.compteur <= 19){         
                        console.log(this.state.compteur)                   
                        t.preventDefault();
                        const i = champ[prop]['key']   //recuperer l'id de la carte
                        const carte = document.getElementById(i)  // recupere la carte
                        carte.className = "carte col-md-3"
                        if(carte.value == "1"){ // Si la carte est dans le deck, pour la remettre en etat initial
                            carte.value = ""
                            document.getElementById('par').appendChild(carte)  // Deplace la carte dans le deck
                            this.state.compteur -= 1
                        }else if (this.state.compteur <= 19){ // Sinon ajout de la carte dans le deck
                            
                            carte.value = "1"
                            document.getElementById('deck').appendChild(carte)  // Deplace la carte dans le deck
                            this.state.compteur += 1
                        }
                            
                        
                    }
                }
            } >

                <Cartes className="" name={champ[prop]['key']} attack={champ[prop]['info']['attack']} defense={champ[prop]['info']['defense']}/></a>
                                )
                

            
        }

            return(
                <div id="par" className=" row">
                    <h2 className="col-12 text-center font-italic"> Cartes </h2>
                    {final}
                </div>
                
                )
        
    }

    render() {
         return(
            <div className="row">

            <div id='init' className="col-md-6">
                 {this.card()}
             </div>

            <div className="col-md-6">
                <div id="deck" className=" row">
                    <h2 className="col-9 text-center font-italic">Deck</h2>
                    <div id="buton" className="col-3">

                    </div>
                    
                </div>

            </div>
        </div>
         )
    }
   

}